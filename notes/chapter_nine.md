# Error Handling

- Rust groups errors into two major categories:
  - _recoverable_ - could be user errors (file not found)
  - _unrecoverable_ - bugs
- Rust does **NOT** have _exceptions_; instead, it has:
  - `Result<T, E>` for recoverable errors
  - `panic!` macro for unrecoverable errors
- By default, when panic, program starts _unwinding_:
  - Rust walks back up the stack and cleans up the data
  - or, _abort_ immediately
    - to abort, add this to your `Cargo.toml`
    ```
    [profile.release]
    panic = 'abort'
    ```
    - to make the binary smaller
  - use _backtrace_ to find out where did the program panic
  - _environment variable_: `RUST_BACKTRACE`
  - read the backtrace from top to bottom
  - `RUST_BACKTRACE=1 cargo run`
  - need to enable the debug mode and by default is **is** enabled
    - when you do `cargo build` or `cargo run` withoud the `--release` flag
- the `Result<T, E>` type is an `enum` with two variants
  - `Ok(T)`
  - `Err(E)`
  - `T` and `E` are generic types
    - `T`: the _type_ of the value that will be returned in a success case
    - `E`: the _type_ of the value that will be returned in a failure case
  - `Result` is actually: `std::result::Result`
  - sometimes we want different actions on different type of errors
- if error occurs in `File::open()`, it's `io::Error`
  - call `kind` method on the error type to get `io::ErrorKind` value
  - `ErrorKind::NotFound`
- **match guard**:
  - example: `Err(ref error) if error.kind() == ErrorKind::NotFound => {}`
- In the context of a pattern
  - `&` matches a reference and gives you its value
  - `ref` matches a value and gives you the reference to it
- `unwrap` method on `Result`
  - returns the value inside `Ok`
  - or, call `panic!` if error
  - use default error message
- `expect` method on `Result`
  - can provide error message
  - `let f = File::open("some.file").expect("Failed to open some.file!");`
- method `read_to_string` reads a file into a string
- propagating the error
- Rust has operator `?`
  - returns the value _inside_ `Ok`
  - returns value _inside_ `Err` **early**
  - **ONLY** works in a func with return type `Result`
  - designed to have the same mechanism as `match`
  - meaning, cannot be used in `main()`
- when code `panic!`, there's no way to recover
- returning `Result` is a good _default_ choice when defining a func that might fail
- `panic!` when _prototyping_
- Error handling guidelines:
  - when performing operations on values, you should verify the values are valid first
  - functions often have **contracts**: requirements on inputs
    - panic when the contract is violated
    - should be documented in API doc
  -
