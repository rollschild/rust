# Notes

- _CANNOT_ assign twice to an immutable variable
- for large data structures, mutating an instance in place may
  be faster than copying and returning newly allocated instances
- *constant*s vs. _immutable_ variables
  - constants are _always_ immutable
  - when declaring a `const`, its type _must_ be annotated
  - constants may be set _only_ to a constant expression
  - `const MAX_POINTS: u32 = 100_000;`
  - constants are valid for the entire time a program runs
- *Shadow*ing
  - by using the same variable's name and repeating the use of
    `let`
  - different than `mut`
    - `let` actually creates a new variable
    - `let` allows you to change the type but use the same name
- _NOT_ allowed to mutate a variable's type
- Rust is _statically typed_
  - it must know the types of _all_ variables at compile time
- Data types:
  - scalar types
    - integer
    - floating-point
    - boolean: `bool`
    - character: `char` - with single quotes
  - compound types - group multiple values into one type
    - tuple
    - array
- `isize` and `usize` depend on arch: 64 bits on 64-bit arch
- `0xff` - Hex; `0o77` - Octal; `0b1111_0000` - Binary
- `57u8` - Decimal
- byte literal: `b'A'` -> `u8` only
- `i32` and `f64` are default integer and floating-point number
- `char` represents a _Unicode Scalar Value_
- _tuple_
  - use `.` followed by index to access tuple elements
- _array_
  - a single chunk of memory allocated on the stack
  - elements must have the same type
  - _fixed length_
    - _CANNOT_ grow or shrink in size once declared
  - useful when you want data allocated on _stack_ rather than
    on _heap_
  - compared to _vector_, which is allowed to grow or shrink
  - when in doubt, use _vector_
  - when trying to access invalid index
    - the compilation would not produce errors
    - but would generate _runtime_ error
- _function_
  - _snake case_: lower case and underscores
  - in function signatures, you _must_ decalre the type of _each_ parameter
  - Rust is _expression-based_
  - _statements_ vs. _expressions_:
    - statements are instructions to perform some action and do _not_ return a
      value
    - expressions evaluate to a resulting value
    - `let x = 6;` is a statement; does not return anything
    - function definitions are also statements
    - expressions do _NOT_ include ending semicolons (`;`)
    - if you add `;` to an expression, you turn it into a statement
  - the return value of a function is synonymous with the value of the final
    expression
  - use arrow `->` to declare return type
- Control flow
  - `if`
    - `if condition { arms } else { arms }`
    - _always_ explicitly provide `if` with a `bool`
    - `if-elseif-else` only executes the block for the first true
      condition and ignores the rest
    - better _not_ to use too many `else if`s - messy code ->
      consider refactoring
    - try using `match`
    - `if` is an expression
    - values that have the _potential_ to be results from each arm
      of `if` must be the same type
      - variables must have a single type
      - Rust needs to know what the type is at compile time
  - `loop` - use `break` to exit the loop
    - `loop`
    - `while` - `while condition {}`
    - `for` - `for element in array.iter() {}`
    - `for` is the most commonly used loop in Rust
      - use `Range`, a type provided by std lib
        - `(begin..end)` - right bound exclusive
