# Generic Types, Traits, and Lifetimes
## Generics
- functions can take parameters of some _generic_ type instead of a concrete type
- Use _traits_ to define behavior in a generic way
- we can define `enum`s to hold generic data types in their variants
  - for example, the `Option<T>` enum
````
enum Option<T> {
    Some(T),
    None,
}
````
  - a generic over type `T` with two variants
  - also, `Result<T, E>`
````
enum Result<T, E> {
    Ok(T),
    Err(E),
}
````
- `powi` 
  - raise an `f32` to an integer power
- generic type parameters in a struct definition are **not** always the same as those you
  use in that struct's method signatures
- **NO** runtime costs by implementing generics
- _Monomorphization_
  - turn generic code into specific code by filling in the concrete types 
    at compile
  - compiler looks at all the places where generic code is called and generates
    code for the concrete types

## Traits
- _trait_ tells the Rust compiler about funcionality a particular type has and can
  share with other types
- we use _traits_ to define shared behavior in an abstract way
- _traits_ are similar to _interfaces_
- A types's behavior consists of the methods we can call on that type
- different types share the same behavior if we can call same methods on all
- _trait_ definitions are a way to group mothod signatures together to define
  a set of behaviors
- Each type implementing a _trait_ **must** provide its own custom behavior for the 
  body of the _trait_ method
  - compiler will enforce this rule
- a _trait_ can have multiple methods
- Restriction to implement `traits`
  - we can implement a trait on a type **only** if either the trait or the type 
    is _local_ to the crate
  - meaning... you **CANNOT** implement external traits on external types
  - this is what we call _coherence_ - the _orphan rule_
- Syntax for overriding a default implementation is the same as that for 
  implementing a trait method without a default implementation
- Default implementations can _call_ other methods in the _same_ trait, eve if 
  those other methods do not have a default implementation
- Place _trait bounds_ with the declaration of the generic  type parameter, 
  after a colon and inside angle brackets
- We can specify _multiple_ trait bounds on one generic type using `+`
````
fn some_function<T: Display + Clone, U: Clone + Debug>(t: T, u: U) -> i32 {}
````
  - alternatively, we can use the `where` clause
````
fn another_function<T, U>(t: T, u: U) -> i32
    where T: Display + Clone,
          U: Clone + Debug
{} // T can be any type that implements Clone and Display
````
- We can also implement a trait for any type that implements another trait
  - _blanket implementation_
````
impl<T: Display> ToString for T {}
// implement the ToString trait on any type that implements Display
````
# Lifetimes
- every reference in Rust has a _lifetime_
  - which is, the _scope_ for which that reference is valid
  - we must annotate lifetimes when the lifetimes references could be related 
    in a few ways
- lifetimes may be Rust's most distinctive feature..?
- main goal: to prevent dangling references
- scope is larger - lives longer
- _borrow checker_
  - compares scopes to determine whether all borrows are valid
- Lifetime Annotation Syntax
  - `'a`
  - place it after the `&` of a reference, with a space in between
````
&'a i32 // lives at least as long as lifetime 'a
````
- lifetime annotations tell Rust how generic lifetime parameters relate to each 
  other
- generic type parameters vs. generic lifetime parameters
- Note: when annotating lifetimes in functions, annotations **only** go in the 
  function signature, **not** in the function body
- `'a` will get the scope of that overlapping between different parameters
  - smallest value
- It's possible to let `struct` hold references
  - _but_ we **must** add lifetime annotations to every reference
  - lifetimes are part of the `struct`'s type
  - **an instance of the `struct` cannot outlive the reference of the what it holds**
- _lifetime elision rules_
- _input lifetimes_ vs. _output lifetimes_
- `'static` - a special lifetime
  - entire duration of the program
  - _all_ string literals
- lifetimes are a type of _generic_ 
