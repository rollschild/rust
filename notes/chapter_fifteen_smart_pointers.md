# Smart Pointers
- smart pointers are usually implemented using `struct`s, but the difference is:
  - smart pointers implement the `Deref` and `Drop` traits
  - `Deref`: behave like a reference!
  - `Drop`: handle out-of-scope situations
## Box<T>
- most straightforward smart pointer, a _box_
- allocate data on **heap**
- **not** on stack
- what remains on stack is the _pointer_ to the heap data
- **NO** performance overhead
- Use `Box<T>` when:
  - having a type whose size unknown at compile time
  - transferring ownership of a large amount of data without copying them
  - you want to own a value and care only that it implements a certain trait, 
    rather than being of a specific type
    - a _trait object_
- At compile time, Rust needs to know how much space a type takes up
- _cons list_
  - `Nil` is **NOT THE SAME** as `Null`
  - they provide _only_ the indirection and heap allocation
  - it's ultimately defined as a tuple struct with one element
- the `Deref` trait
  - you can customize `*`, the _dereference operator_
  - `let y = &x;` === `let y = Box::new(x);`
  - treating a type like a reference by implementing 
    the `Deref` trait
  - without the `Deref` trait, the compiler can dereference 
    only `&` references
  - _Deref coercion_
    - convert a reference to a type that implements `Deref` 
      into a reference to a type that `Deref` can convert the 
      original type into
    - immutable references will **never** coerce to mutable ones
- `Box<T>` customizes `Drop` to deallocate the space on heap that the box points to
- the `Drop` trait
  - requires you to implement the `drop` method
  - , which takes a mutable reference to `self`
  - the `Drop` trait is included in the prelude - no need to import
  - sometimes you want to clean up a value _early_
  - you **cannot** manually call `Drop`'s `drop` method
  - you need to call `std::mem::drop` instead
  - _destructor_
  - the `drop` function is one particular destructor
## Rc<T>
- a single value can have multiple owners
- in graph data structures, multiple edges might point to the same node
- `Rc<T>` enables multiple ownership
- _reference counting_
- it keeps track of the number of references to a value
- **ONLY FOR SINGLE-THREADED SCENARIOS**
- prefer `Rc::clone(&stuff)` to `stuff.clone()` under this situation
- `Rc::clone()` does **not** make a deep copy
- to get reference count, use `Rc::strong_count()`
- we also have `Rc::weak_count()` - obviously...
- `Rc<List>` has an initial reference count of 1
- every time `Rc::clone()` is called, reference count goes up by 1
- when corresponding variable goes out of scope, count goes down by 1
  - realized by the `Drop` trait
- **immutable** references - read only
## RefCell<T> and Interior Mutability
- _interior mutability_ is a design pattern,
  - which allows you to mutate data when there are immutable references to that data
  - **unsafe**
- `RefCell<T>` represents single ownership over the data it holds
- differences from `Box<T>`:
  - `Box<T>`: borrowing rule enforced at compile time
  - `RefCell<T>`: borrowing rule enforced at run time
- static analysis, like Rust's compiler, is inherently conservative
- also **ONLY FOR SINGLE THREADED SCENARIOS**
- General rules:
  - when you have an immutable value, you **cannot** borrow it mutably
- two methods:
  - `borrow_mut()` - returns smart pointer type `Ref<T>`
  - `borrow()` - returns `RefMut<T>`
  - they are part of the safe API
- `usize`: pointer-sized unsigned integer type
- it keeps track of how many `Ref<T>` and `RefMut<T>` are active
- other types that provide interior mutability:
  - `Cell<T>`
  - `Mutex<T>`
## Reference Cycle Can Leak Memory
- _memory leak_
  - memory that is never cleaned up
- memory leak is memory safe in Rust
- reference cycle can cause a memory leak
- the keyword `ref`
- `Rc::clone()` increases `strong_count` of an `Rc<T>` instance
- an `Rc<T>` instance is _only_ cleaned up if its `strong_count` is zero
- `Weak<T>`
  - to create a _weak reference_
  - ...by calling `Rc::downgrade` and passing a ref to `Rc<T>`
  - to use the weak reference, call `upgrade()` on it, which
  - ...returns an `Option`
  - `weak_count` does not need to be zeor for `Rc<T>` to be cleaned up
- a parent node should own its children:
  - dropping the parent node should also drop its children
- **BUT** children should _not_ own their parent
