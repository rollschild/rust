# Closures
- To define a closure, start with a pair of vertical pipes:
  - `let closure = |num| {};`
  - , in which we specify the parameters to the closure
- closure contains the **definition** of an anonymous function, _not_ the resulting
  value of calling the anonymous function
- closure does **not** require type annotations of parameters or return values
  - because closures are **not** used in an exposed interface
  - they are stored in variables and used without naming them or exposing them
    to users
- closures are often short and relevant only within a narrow context rather
  than in any arbitrary scenario
- you _can_ add type annotations if you want...
- closure's annotation type is locked once it's set
  - meaning, you **CANNOT** call the same closure twice with different parameter 
    types or return types
- the `Fn` traits
  - _all_ closures implement _at least_ one of the following traits:
    - `Fn`
    - `FnMut`
    - `FnOnce`
  - functions implement _all_ three `Fn` traits
- closures can capture their environment and access variables from the scope 
  in which they are defined
  - it uses memory to store the captured values
  - , in three ways:
    - taking ownership: `FnOnce`
    - borrowing mutably: `FnMut`
    - borrowing immutably: `Fn`
  - _all_ closures implement `FnOnce` because they can all be called at least once
  - to _force_ the closure to take ownership, use the `move` keyword before the 
    parameter list
    - most useful when passing a closure to a new _thread_ to move the data so 
      that the data is owned by new _thread_
    - concurrency maybe?

# Iterators
- in Rust iterators are **lazy**
- `Vec<T>.iter()` - this does not do anything (yet) upon declaration
- _new syntax here!_
  - `type Item;`
  - `Self::Item`
  - above are _associated types_
````
pub trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;
}
````
- the `Iterator `trait only requires implementors to difine one method: `next()`
- the values we get from calls to `next()` are _immutable_ references to the values
  in the vector
- the `iter` method produces an iterator over immutable references
- `into_iter`: takes ownership and returns owned values
- `iter_mut`: iterate over mutable references
- _consuming adaptors_:
  - methods that call `next`
  - eg. the `sum()` method
    - it takes ownership of the iterator and iterates through
    - by repeatedly call `next()`, thus consuming the iterator
- other methods defined on the `Iterator` trait are _iterator adaptors_
- you can chain multiple _iterator adaptors_ but you have to call a
  _consuming adaptor_
````
let v1: Vec<i32> = vec![4, 5, 6];
let v2: Vec<_> = v1.iter().map(|item| item + 1).collect();
assert_eq!(v2, vec![5, 6, 7]);
````
- the `map()` above creates a new _iterator_
- to implement your own `Iterator` trait, the **only** method you **must** provide is the 
  `next()` method
- `zip` and `skip`
- `std::env::Args` implements the `Iterator` trait
  - meaning, we can call `next()` on it
- Iterators are Rust's _zero-cost abstractions_
- _Unrolling_: generate repetitive code instead of the overhead of looping
-
