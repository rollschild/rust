# Concurrency
- _threads_ manage the different parts running independently inside a program
- problems:
  - race conditions
  - deadlocks
  - bugs that are specific to certain conditions
- _1:1_ thread:
  - one operating system thread per one language thread
- _M:N_ thread:
  - _M_ green threads per _N_ operating system threads
  - _green threads_: programming-language-provided threads
- runtime:
  - the code that's included by the language in every binary
  - every non-Assembly language has some runtime
  - the _green threading_ _M:N_ model requires larger runtime to manage threads
  - Rust std lib **only** provides _1:1_ threading
- to create a thread, call `thread::spawn` and pass a _closure_ to it
- `thread::spawn` returns `JoinHandle`
- calling `join` on the `handle` blocks the thread _currently running_ until
  the thread represented by the handle terminates
- the `move` keyword is useful when creating new threads in order to transfer 
  ownership of values from one thread to another
- _channel_:
  - for passing messages
  - transmitter and receiver
  - a channel is _closed_ if either the transmitter or the receiver is dropped
  - `std::sync::mpsc`
  - `mpsc`: multiple producers, single consumer
````
let (tx, rx) = mpsc::channel();
````
  - Rust's stdlib implmentation: a channel can have multiple 
    senders and one receiver
  - the spawned thread needs to own the transmitting end of the channel
    to be able to send message through the channel
  - both `send()` and `recv()` return a `Result<T, E>`
  - `try_recv()`
  - takes ownership!
- _Mutex_
  - mutual exclusion
  - allows _only one_ thread to access some data at any given time
  - a thread must first ask for mutex's _lock_
  - Two important rules:
    - you must attempt to acquire the lock before using the data
    - you must unlock the data after you are done with it
  - the `lock()` call returns a mutable reference
    - more accurately, it returns a smart pointer `MutexGuard`
    - it implments both `Deref` and `Drop`
    -
  - `Mutex<T>` is a smart pointer
  - You **cannot** move ownership of a `Mutex` into multiple threads
  - `Rc<T>` is **not** safe to share across threads
  - `Arc<T>` to the rescue! 
    - `A` for _atomic_
    - _atomically reference counted type_
    - `std::sync::atomic`
    - safe to share across threads
    - thread safety comes with a performance penalty!
    - `Mutex<T>` provides interior mutability
    - you have the risk of creating _deadlocks_!
    - _deadlock_:
      - when an operation needs to lock two resources
      - ...and two threads have each acquired one of the locks,
      - ...causing them to wait for each other forever
- the `Send` marker trait
  - `std::marker`
  - ownership of the type implementing `Send` can be transferred between threads
  - _almost_ every Rust type is `Send`, but `Rc<T>` is one of the exceptions
  - _almost_ all primitive types are `Send`, except raw pointers
- the `Sync` marker trait
  - it's safe for types implementing `Sync` to be referenced from multiple threads
  - any type `T` is `Sync` if `&T` is `Send`
  - ...meaning, the reference can be sent safely to another thread
  - primitive types are `Sync`
  - `Rc<T>`, `RefCell<T>` are **not** `Sync`
  - `Mutex<T>` is `Sync`
