# Notes

- `struct` - similar to _tuple_
  - to use `struct`, we create *instance*s with `key: value` pairs
  - `struct_instance_name.field_name` to access data
  - _CANNOT_ mark only certain fields mutable
- _field init shorthand_ syntax:
  - when parameter name is same as the struct field name
- _struct update syntax_ :
  - `..`
  - `let user2 = User {..user1};`
- _tuple struct_
  - `struct Color(i32, i32, i32);`
- each `struct` you define is its own type
- _unit-like struct_
  - `struct` that does not have any fields
  - behave like `()` - the _unit type_
- `struct` is able to store references, but need *lieftime*s
- `struct` does _not_ have an implementation of the `Display` output format
  - we could use the `Debug` format: `{:?}`
  - but we need to enable it manually: use `#[derive(Debug)]` _just before_
    the definition
  - `{:#?}`: prettier?
- *Method*s
  - they are defined within the context of
    - a `struct`, or
    - an `enum`, or
    - a _trait_ object
  - first parameter is _always_ `self`
    - which represents the instance of the `struct` the method is being
      called on
  - _method syntax_:
    - `struct_instance.method_name();`
  - if we want to write to instance: `&mut self`
- Rust has a feature _automatic referencing and dereferencing_

```
p1.distance(&p2);
/* is equivalent to */
(&p1).distance(&p2);
```

- *Associated function*s
  - can also be defined inside `impl` block
  - do _not_ take `self` as a parameter
  - example: `String::from()`
  - often used for constructors to return a new instance of the `struct`
  - to access the function, use `::`
  -
