# Notes

- `enum` in Rust is similar to _algebraic data types_ in OCaml and Haskell
- `enum` values can only be one of the variants
- for an `enum`, each variant can have different types and amounts of
  associated data
- in fact, you can put any kind of data inside an `enum` variant
- we are also able to define *method*s on `enum`s
- `Option`: an `enum` defined in standard library

```
enum Option<T> {
    Some(T),
    None,
}
```

- `<T>`: _generic type parameter_
- if we use `None` rather than `Some`, we need to specify what type of `Option<T>`
- Why is `Option<T>` better?
  - `Option<T>` and `T` are different types
  - _CANNOT_ use `Option<T>` as a valid type
  - _you have to_ convert `Option<T>` to `T` before performing `T` operations with it
- the `match` control flow
  - consists of *arm*s:
    - a pattern
    - some code
  - code associated with each arm is an expression
  - ...resulting value of the expression in the mathing arm is the return value
    for the entire `match` expression
  - match arms can bind to the parts of the values that match the pattern
    - how we extract values out of the variants
- can also use `match` to extract the inner `T` of `Option<T>`
- we _have to_ handle both `Some` and `None`
- *match*es are _exhaustive_:
  - every possiblity must be covered
- the `_` placeholder
  - `_ => ()`
- the `if let` syntax
  - for the case where you want one pattern and ignoring the rest
  - syntax sugar for `match`
  - `else`
