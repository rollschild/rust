# OOP
- Rust does not have _inheritance_
- inheritance: 
  - code sharing
  - type system: to enable a child type to be used in the same palces as the 
    parent type
    - a.k.a. _polymorphism_
- Rust uses _generics_ and _traits_: this is called _bounded parametric polymorphism_
- _trait object_ points to an instance of a type that implements 
  the trait we specify
- `trait` objects are similar to the _objects_ concept in other OOP languages,
  that they combine data and behavior
  - but we **cannot** add data to a `trait` object
- **New syntax for trait object!**
  - `dyn trait` 
    - new way to use trait objects
    - to avoid ambiguous
  - `impl Trait`
    - new way to specify unnamed but concrete types that implement a certain trait
```` 
trait Trait {}

fn foo<T: Trait>(arg: T) {
}

fn foo(arg: impl Trait) {
}
````
- _static dispatch_ vs. _dynamic dispatch_
  - Rust uses static dispatch when using _trait bounds_
  - use dynamic dispatch when using _trait objects_
  - dynamic dispatch prevents inlining a method's code -> prevents optimization
- object safety rules:
  - the return type is **not** `Self`
  - there are _no_ generic type parameters
- `push_str`: append string slice to `String`
- `std::option::Option.take()`: takes value out of `Option` and leaves a `None`
- `as_ref()` on `Option`: return a reference to the value inside `Option`
- 
