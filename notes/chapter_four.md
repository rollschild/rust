# Notes

- Ownership allows memory safety _without_ needing a garbage collector
- Ownership is Rust's central feature
- memory is managed through a system of ownership with a set of rules that the
  compiler checks at compile time
- _stack_ vs. _heap_
  - all data on stack must take up a _known_, _fixed_ size
  - data with unknown size at compile time or a size that might change can be
    stored on heap
  - _allocating_ on heap
  - pusing values onto _stack_ is _not_ allocating
- _Ownership rules_
  - Each value in Rust has a variable that's called its _owner_
  - There can be _only one_ owner at a time
  - When owner goes out of scope, the value will be dropped
- type `String` is allocated on heap if mutable
  - memory requested from OS at runtime
  - need to return the memory to OS when it's done with `String`
- need to pair exactly one `allocate` with one `free`
- Rust:
  - the memory is automatically returned once the variable that
    owns it goes out of scope
  - to do this, Rust calls `drop()`
  - called _Resource Acquisition Is Initialization (RAII)_ in
    C++
- a `String` is made up of three parts:
  - a _pointer_ to the memory (on _heap_) that holds the content
    of the string
  - a _length_
  - a _capacity_
  - this group of data is on stack
  - content of `String` is on _heap_
- when `String` data is copied: pointer, length, and capacity on
  the stack are copied, to stack
  - data on heap that the pointer refers to is _NOT_ copied
  - can lead to _double free_ vulnerabilities
  - _IMPORTANT_: in the mean time Rust _invalidates_ the first
    variable
  - it's called a _move_ - similar to _shallow copy_ but different
- Rust _never_ automatically create _deep copy_
- `clone()` does a _deep copy_
  - _heap_ does get copied
  - _expensive_ operation
- types such as _integer_ have a known size at compile time and
  is stored entirely on stack
  - _shallow copy_ and _deep copy_ are same
  - this kind of type has a _Copy_ trait
- _cannot_ add _Copy_ trait if the type (or any of its parts)
  has implmented the _Drop_ trait
- simple scalar types (don't need allocation) are _Copy_
- *tuple*s are _Copy_ _if and only if_ types contained inside are also _Copy_
- passing a value to a function will _move_ or _copy_
  - passing a `String` to a function will take away the ownership
  - passing simple scalar type will _copy_ instead of _move_
- assigning a value to another variable moves the ownership
- when a variable with some data on heap goes out of
  scope, the value will be cleaned up by `drop` unless the
  data has been moved to be owned by another variable
- Rust is able to return multiple values from one function
- pass _reference_ to a variable to the function - avoid taking ownership
  - _reference_ allows you to refer to some value without taking ownership of it
  - `&variable` creates a reference that refers to `variable` but does _not_ own it
  - when the reference goes out of scope, what it points to does _not_ get dropped
  - _borrowing_: references as function parameters
- _reference_ is _immutable_ by default
  - not allowed to modify something we have a reference to
  - fix this by using `mut`
- _mutable reference_:
  - change variable to be `mut`
  - create a _mutable reference_ to variable with `&mut variable`
  - function has to accept _mutable reference_
  - one _restriction_:
    - _only one_ mutable refernce to a piece of data in a particular scope
    - prevent data races at compile time
- _data race_ happens when:
  - two or more pointers access the same data at the same time
  - at least one of the pointers is writing to the data
  - no mechanism to synchronize access to the data
- we can have multiple _immutable_ references to the same data
- but we _cannot_ have _immutable_ and _mutable_ references to the same data at
  the same time
- mutilple _immutable_ references to the same data is ok
- _slice_ does not have ownership
  - reference a contiguous sequence of elements in a collection
- `.iter().enumerate()` returns a tuple of `(index, &reference)`
- *string slice*s:
  - the `..` range syntax
  - `let slice = &str[..];` - entire string
  - its type is `&str`
  - _NOTICE_: string slice has its own type!
- _string literals are slices_ - immutable
- _pass string slice to function_ is a better way
