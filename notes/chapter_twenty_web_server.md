# Build a single-threaded web server
- Two protocols:
  - **HTTP** - _Hypertext Transfer Protocol_
  - **TCP** - _Transmission Control Protocol_
- Both are _request-response_ protocols
  - client initiates the request
  - ...server listens to the request then provides a response
  - TCP is the lower-level protocol that describes the details of how 
    information gets from one server to another
  - ...but TCP doesn't specify what the information is
  - HTTP, usually, builds top top of TCP
  - ...by defining the contents of the requests and responses
- use `std::net` to deal with TCP
- for HTTP, binding to port 80 requires admin privileges
- non-admin can only bind to ports higher than `1024`
- a single `stream` represents an open connection between client and server
- _connection_: the full request-and-response process where 
  - a client connects to server
  - server generates a response
  - server closes the connection
- you might see multiple messages for one browser reuest:
  - browser is making other requests in addition to requesting the web page
  - for example the `favicon.ico`
  - or, it might be the browser is trying to connect multiple times because the 
    server isn't responding with any data
- `TcpStream` keeps track of what data it returns to us, internally
  - it might read more data than we asked for
  - it might save the extra data for the next time we want 'em
- _URI_: Uniform Resource Identifier
- _URL_: Uniform Resource Locator
- HTTP uses _URI_
- first line of an HTTP request:
  - `Mthod Request-URI HTTP-Version CRLF`
  - `CRLF`: carriage return and line feed
- `as_bytes`: convert string data to bytes
- the `write` method on `stream` takes a `&[u8]` and sends those bytes directly
  down the connection
  - but does _not_ print to std output
- `flush()` waits and prevents the program from continuing until all the bytes 
  are written to the connection
- Improve throughput with a thread pool
  - _thread pool_
    - a group of spawned threads that are waiting and ready to handle a task
    - allows you to process connections concurrently, increasing throughput
    - pay attention to DoS attacks!
  - the pool will remain a queue of incoming requests
  - each thread in the pool pops off a request from the queue, handles the 
    request, then asks the queue for another one
  - we can process N requests concurrently, where N is number of threads
  - Other options:
    - fork/join model
    - single-threaded async I/O model
  - How to design the code:
    - write the client interface first
    - write the API so it's structured in the way you want to call it
    - ...then implement the functionality within that structure
    - compiler-driven development
    - `cargo check`
    - `usize`: non-negative number
    - `spawn` uses `FnOnce` as the trait bound on `F`
````
pub fn spawn<F, T> -> JoinHandle<T>
    where 
        F: FnOnce() -> T + Send + 'static,
        T: Send + 'static
````
    - `F` type parameter also has trait bound `Send` and lifetime bound `'static`
      - we need `Send` to transfer the closure from one thread to another
      - `'static`: we don't know how long the thread will execute
    - `thread::spawn()` creates a thread but expects to get some code and run it
      as soon as the thread is created
    - as a thread pool, we want to create the threads and wait for code that's
    - sent later
    - a new data structure called `Worker` as a manager between `ThreadPool` and 
      threads
      - a common term in pooling implementations
    - store instances of the `Worker` struct inside `ThreadPool`
- run `cargo doc --open` to open up your own documentation!!!
- `Vec::with_capacity()`: preallocates space in the vector
- `Vec::new()`: resizes itself as elements are inserted
- Send requests to threads via _channels_
  - Blueprint:
    - `ThreadPool` creates a channel, acting as the sending side
    - each `Worker` as receiving side
    - we create a new `Job` struct that holds the closures we want to send down 
      the channel
    - the `execute` method sends the job it wants to execute down the sending 
      side of the channel
    - inside thread, the `Worker` loops over its receiving side and executes the 
      closures of any jobs it receives
  - Rust's channel implementation: _multiple_ producers, _single_ consumer
  - we want to distribute the jobs across threads by sharing the single `receiver`
    among all workers
  - use `Arc<Mutex<T>>`:
    - `Arc` allows multiple ownership
    - `Mutex` ensures only one worker gets a job from the receiver at a time
  - we want to call the `FnOnce()` closure inside `Box<T>` - need to move it out of `Box`
  - `FnOnce`: closure can only be called once
  - to move `self` out of `Box<T>`: use dereference operator `*`
    - `(*self)()`
- Graceful shutdown and cleanup
  - doing `CTRL-C` forces all threads to quit immediately
  - need to implement the `Drop` trait on the thread pool
  - the pool is dropped, all threads should join to make sure they finish all work
  - `thread.join()` takes ownership
  -
