# Unsafe Rust
- static analysis is conservative
- the underlying computer hardware is inherently unsafe
- to use _unsafe_ Rust, use the `unsafe` keyword then start a new block
- **Unsafe Superpowers**:
  - dereference a raw pointer
  - call an unsafe function or method
  - access or modify a mutable static variable
  - implement an unsafe trait
- You _will_ know that any errors related to memory safety **must be** within an 
  `unsafe` block
- **KEEP UNSAFE BLOCKS SMALL**
- it's best to enclose unsafe code within a safe abstraction and provide a safe
  API
- dereferencing a raw pointer
  - raw pointers ignore the borrowing rules:
    - you can have both immutable and mutable pointers or multiple mutable 
      pointers to the same location
  - raw pointers are **not** guaranteed to point to valid memory
  - are allowed to be null
  - do **not** have any automatic cleanup
  - `*const T` - immutable raw pointer
  - `*mut T` - mutable raw pointer
````
let mut num = 2037;
let raw_one = &num as *const i32;
let raw_two = &num as *mut i32;

unsafe {
    println("raw_one is: {}", *raw_one);
    println("raw_two is: {}", *raw_two);
}
````
  - we _can_ create raw pointers in safe mode
  - ...just **cannot** dereference them outside of an `unsafe` block
  - useful when interfacing with C code (I knew it!)
- calling unsafe function/methods
  - unsafe function/method has an `unsafe` keyword before the definition
````
unsafe fn dangerous() {}
unsafe {
    dangerous();
}
````
  - the `unsafe` funtion body is effectively an `unsafe` block - you can do unsafe
    operations inside without adding another `unsafe` block
  - wrapping unsafe code in a safe function is a common abstraction
- `slice.as_mut_ptr()` - get the raw pointer of a slice
- `std::slice::from_raw_parts_mut(ptr, len)`
  - takes a raw pointer and a length
  - creates a slice
  - -> create a slice starting from `ptr` and is `len` items long
- `ptr.offset(size as isize)`
  - get a raw pointer that starts at `size`
- both `from_raw_parts_mut` and `offset` are _unsafe_
- Rust has keyword `extern` to interact with another language
  - use _Foreign Function Interface (FFI)_
  - _FFI_ - a way for a language to define functions and enable a different 
    language to call those functions
  - functions declared within `extern` are _always unsafe_
````
extern "C" {
    fn abs(input: i32) -> i32;
}
````
  - the `"C"` part defines which _application binary interface (ABI)_ the external
    function uses
  - the _ABI_ defines how to call the function at the assembly level
  - Can also use `extern` to create an interface that allows other languages to
    call Rust functions
    - -> specify the `extern` keyword and the ABI right before `fn`
````
#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Just called some Rust function from C!");
}
````
  - `#[no_mangle]` tells Rust compiler not to mangle the function name
  - mangle/mangling: when compiler changes the name that we've given to a 
    function to a different one
    - the name might contain more information for other parts of the compilation
      process
    - but the name might be less human readable
- accessing/modifying a mutable static variable
  - in Rust, global variables are called _static variables_
  - the name of a static variable is in _SCREAMING_SNAKE_CASE_ by convention
  - we **must** annotate its type
  - static variables can **only** store references with the `'static` lifetime
  - accessing an immutable static variable is safe
  - values in a static variable have a fixed address in memory - using the value
    always accesses the same data
  - static variables can be mutable - but _unsafe_
  - _any_ code that reads/writes from a mutable static variable **must be** within an 
    `unsafe` block
  - better to use concurrency techniques
- Implementing an unsafe trait
  - `unsafe trait Foo {}`
  - `unsafe imple Foo for Bar {}`
# Advanced Lifetimes
- _lifetime subtyping_ specifies that one lifetime should outlive another lifetime
  - one lifetime parameter lives at least as long as 
    another one
- _lifetime bounds_: lifetime parameters as constraints on
  generic types
  - verify that references in generic types will **not** 
    outlive the data they're referencing
```` 
struct Ref<'a, T: 'a>(&'a T);
// if T contains any references, they must live at least
// ...as long as 'a
````
  - alternatively,
````
struct StaticRef<T: 'static>(&'static T);
````
- Trait object lifetimes
  - default lifetime of a trait object is `'static`
  - with `&'a Trait` or `&'a mut Trait`, the default lifetime
    of teh trait object is `'a`
  - with a single `T: 'a` clause, default lifetime of the trait
    object is `'a`
  - if there're multiple clauses like `T: 'a`, no default lifetime exists - 
    we must specify explicitly
# Advanced Traits
- -> Specify placeholder types in trait definitions with associated types
- _Associated Types_
  - connect a type placeholder with a trait
  - ...so that, trait method definitions can use these placeholder types in 
    their signatures
  - associated types vs. generics
    - there can **only** be one `impl Iterator for Counter`
    - there can be multiple `impl Iterator<T> for Counter`
    - when a trait has a generic parameter, it can be implemented for a type 
      multiple times, changing the concrete types of the generic type parameters
      each time
    - when using the `next` method on `Counter`, we would have to provide type
      annotations to indicate which implementation of `Iterator` we want to use
  - Default generic type parameters
    - syntax: `<PlaceholderType=ConcreteType>`
    - _Operator Overloading_: customize the behavior of an operator
    - Rust does **not** allow creating your own operators or overloading arbitrary
      operators
    - _But_ you can overload the operations and corresponding traits listed in 
      `std::ops` by implementing the traits associated with the operator
````
trait Add<RHS=Self> {
    type Output;

    fn add(self, rhs: RHS) -> Self::Output;
}
````
    - `RHS=Self`: default type parameters
  - `StructName::method_name(&instance)` is equivalent to
    `instance.method_name()`
  - Fully Qualified Syntax:
    - useful when calling _Associated functions_
    - assocated functions do *not* have a `self` parameter
    - `<Type as Trait>::function(receiver_if_method, next_arg, ...);`
  - _Supertrait_
    - `trait OutlinePrint: fmt::Display` - the `OutlinePrint` trait **only** works
      for types that also implement `Display`
  - External traits on external types
    - We are allowed to implement a trait on a type as long as either the trait 
      or the type are local to our crate
    - _newtype pattern_
    - create a new type in a _tuple struct_
    - the tuple struct has only one field and is a thin wrapper around the type 
      we want to implement a trait for
    - _newtype_ is from Haskell!
    - No runtime penalty!!!
    - `struct Wrapper(Vec<String>);`
    - use `self.0` to access the inner type
    - Downside of this: `Wrapper` is a new type, so it doesn't have the methods 
      of the value it's holding
      - we would have to implement all methods of `Vec<T>` directly on `Wrapper`
        such that the methods delegate to `self.0`
      - if we want the new type to have _every_ method its inner value has, 
        implement the `Deref` trait
      - if we want to restrict the methods the wrapper can have, we need to 
        implement just the methods we do want, manually
# Advanced Types
- Create type synonyms with type aliases
  - `type Kilometers = i32;`
  - now `Kilometers` is a synonym to `i32`
  - _thunk_: code to be evaluated at a later time
  - I/O operations often return a `Result<T, E>` to handle situations when 
    operations fail
    - `E` is `std::io::Error`
    - `std::io` has the type alias `type Result<T> = Result<T, std::io::Error>;`
  - a special type `!`
    - the empty type - it has no values
    - we call it _never type_
    - as a return type of a function - the funtion **never** returns
    - functions that return _never_ are called **diverging functions**
    - `continue` has a `!` value
    - *expressions of type `!` can be coerced into any other type*
    - `panic!` also has type `!`
    - `loop` has type `!` as well
  - Dynamically sized types
    - a.k.a. _DSTs_ or _unsized types_
    - type is known _only_ at runtime
    - `str` is a DST
      - **NOT** `&str`
      - we don't know how long the string is until runtime
      - all values of a particular type must use the same amount of memory
      - `&str` is actually two values: the address of `str` and its length
      - size of `&str` is twice the size of `usize`
      - Rule: we must _always_ put values of dynamically sized types behind a 
        pointer of some kind
      - every _trait_ is a dynamically sized type
      - Rust has the `Sized` trait specifically for DSTs
        - automatically implemented for everything whose size is known at 
          compile time
        - Rust implicitly adds a bound on `Sized` to every generic function
        - by default, generic functions will work only on types that have a 
          known size at compile time
        - use this:
````
fn generic<T: ?Sized>(t: &T) {
  // whatever...
}
````
        - a trait bound on `?Sized`: `T` may or may not be `Sized`
        - this syntax is **ONLY** for `Sized`
# Advanced Functions and Closures
- the `fn` type: _function pointer_
- function pointers implement all three closure traits: `Fn`, `FnMut`, and `FnOnce`
- you can always pass a function pointer as an argument for a function that 
  expects a closure
````
let list_of_numbers = vec![5, 99, 13];
let list_of_strings: Vec<String> = list_of_numbers
    .iter()
    .map(ToString::to_string)
    .collect();
// must use *fully qualified syntax* to call to_string()
// trait ToString is implemented by the standard lib 
// ...for any type that implements Display
````
- closures are represented by traits - you **CANNOT** directly return closures
````
fn returns_closure() -> Box<Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}
````

