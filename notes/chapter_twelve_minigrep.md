# I/O Project
- `std::env::args` - to read values of command line arguments
  - returns an _iterator_ of the arguments
  - we can call `collect()` method to assemble all into a _vector_
  - _will panic_ if there're invalid Unicode arguments
    - use `std::env::args_os` instead
    - iterator produces `OsString`, instead of `String`
    - platform dependent
- `std::io::prelude::*` brings common types and functions for I/O into scope
- possible `index out of bounds` error
-
# Project Structuring Rules
- Split into `main.rs` and `lib.rs`; move logics to `lib.rs`
- As long as command line parsing logic is small, keep it in `main.rs`
- If command line parsing logic gets complicated, move it to `lib.rs`
- What's left in `main.rs`
  - Call command line parsing logic
  - Set up configuration
  - Call a `run` function from `lib.rs`
  - Error handling for `run`
- Looks like you **cannot** test `main.rs` directly
- `&'static str` is the type for string literals
- `unwrap_or_else()` - define custom and non-`panic!` error handling
- _Non-zero exit status_ - program exited with an error state
- _unit type_ - `()`
- _trait object_: `Box<Error>`
  - a type that implements the `Error` trait
- `to_lowercase()` creates new data rather than referencing existing data
- to have environment variables, run
````
ENV_VAR=some_value cargo run [..arguments]
````
- `eprintln!` - print to `stderr`
- `if let`:
  - **The `if let` construct reads: 
    - "if `let` destructures `number` into `Some(i)`, evaluate the block (`{}`).
