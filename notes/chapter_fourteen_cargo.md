# Release
- _release profiles_
- `cargo build` (dev) and `cargo build --release`
- `[profiles.*]` section in `Cargo.toml`
  - `opt-level`: 0 - 3
  - for dev, default is 0
  - for release, default is 3
- _documentation comment_: `///`
  - generates HTML documentation
  - run `cargo doc` to generate documentation
  - ...which runs `rustdoc`, and places doc into `target/doc` directory
- if documentation has example code blocks, running `cargo test` will run those
  as tests!!!
- expose public API:
  - `pub use mod_name::some_example;`
- `cargo publish`
- a publish is **permanent**
- you **CANNOT** remove previous versions of a crate from `crates.io`
- however, you can prevent any _future_ projects from adding them as a new 
  dependency
  - `cargo yank --vers 1.2.1`
- Workspace:
  - A workspace is a set of packages that share the same `Cargo.lock` and output
    directory
  - **However, they share different `Cargo.toml` files**
    - which, does *not* have a `[package]` section
    - instead, it has a `[workspace]` section
````
[workspace]
members = [
    "adder",
    "another_package",
]
````
    - then we add the `adder` binary crate within the workspace directory
    - then build the workspace by `cargo build`
    - all packages/crates share a single `target` directory at the root level of
      the workspace
    - Cargo **does not** assume crates in one workspace will depend on each other
    - ...if they do, you **have to** explicitly add the dependencies in each
      crate's `Cargo.toml` file
```` 
/* adder/Cargo.toml */
[dependencies]
another_package = {patch = "../another_package"}
````
  - run `cargo build`  at the top level
  - to run a specific binary crate, run `cargo run -p adder`
  - all crates in the workspace use the same dependencies (as for versions etc.)
  - `cargo test` runs tests from _all_ crates, at the root level
  - use `cargo test -p` to run tests for a specific crate
  - `cargo publish` **does not** publish _all_ crates
  - you need to publish each crate separately
- `cargo install`
-
