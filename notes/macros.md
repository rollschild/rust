# Macros
- _macros_ are a way of writing code that writes other code
- called **metaprogramming**
- macros _expand_ to produce more code
- macros can take a _variable_ number of parameters
- macros are expanded _before_ the compiler interprets the meaning of code
- for macros defined in external crates, you have to explicitly bring them into
  scope:
````
#[macro_use]
extern crate crate_name;
````
- you must define/bring macros into scope **before** calling them in file
- `!` is used to expand a macro
- Two kinds of macros: _declarative/macro_rules!_ macros & _procedural_ macros
- use `macro_rules!` to define a macro
- a simplified version of defining macro `vec`:
````
#[macro_export]
macro_rules! vec {
    ($($element:expr),*) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($element);
            )*
            temp_vec
        }
    };
}
````
- `#[macro_export]`: this macro should be made available whenever the crate is 
  imported
  - without it, even with `#[macro_use]` the macro still would not be available
- syntax is similar to the `match` arm
- more complicated macros will have more than one arm
- dollar sign `$` followed by a set of parentheses `()` captures the value that 
  matches the pattern
- `expr` - any Rust expression
- _Procedural_ macros are more like functions
  - they accept some Rust code as input, operates on those code, and generates
    some Rust code as output
  - ...rather than matching against patterns and replacing the code
- procedural macros can be defined to allow your traits to be implemented on a 
  type by specifying the trait name in a `derive` annotation
- Rust does **not** have reflection capabilities - it **cannot** look up the type's name 
  at runtime
- Procedural macros need to be in their own crate
  - this restriction might be lifted in the furture
- for a crate named `foo`, a custom derive procedural macro crate is called `foo_derive`
-
