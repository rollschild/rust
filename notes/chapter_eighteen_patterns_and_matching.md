# Patterns and Matching
- match expressions need to be _exhasustive_:
  - **all** possibilities must be accounted for
  - use `_` as a catchall pattern for the _last_ arm
- `while let`
  - the conditional loop keeps running for as long as the pattern continues
    to match
  - use `while let` to pop every element off a stack
- `for` loop:
  - the pattern is the value diectly following keyword `for`
- `let` statement is also a pattern
````
let PATTERN = EXPRESSION;
// bind what matches EXPRESSION here to variable PATTERN
````
````
let (x, y, z) = (4, 5, 6);
````
- Refutability
  - whether a pattern might fail to match
  - _irrifutable_: 
    - pattern will match for any possible value passed
    - `let x = 2037;`
  - refutable:
    - pattern might fail for some value
    - `if let Some(val) = some_val;`
  - function parameters, `let` statements, and `for` loops can _only_ accept 
    irrefutable patterns
  - `if let` and `while let` _only_ accept refutable patterns
  - `match` arms must use _refutable_ patterns, **except for** the last arm, which 
    should use _irrifutable_ patterns
- match multiple patterns using the `|` syntax
- match a range: `1 ... 6`
  - only numeric or `char` values are allowed here
- destructuring references:
  - by specifying a `&` in the pattern
  - get a variable that holding the value that the reference points to
  - ...rather than getting a variable that holds the reference
- you can ignore an unused variable by starting its name with `_`
  - for example, `let _x = 5;`
- the `_var_name` still binds the variable, but `_` does **not** bind at all
- ignore remaining parts of a value with `..`
- creating references in patterns
  - the `&` syntax in patterns does **not** create a reference, but **matches** an 
    existing reference in the value
  - to create a reference in a pattern, use the `ref` keyword before the new 
    variable
  - ...and `ref mut` -> use the `*` operator to dereference and mutate the value
- _match guards_:
  - and additional `if` condition specified after the pattern in a `match` arm
  - must also match
  - you can use the _or_ operator `|` in a match guard to specify multiple patterns
    - the match guard condition will apply to all the patterns
- the `@` bindings:
  - create a variable that holds a value at the same time while testing that 
    value to see whether it matches a pattern
````
match msg {
    Message::Hi {id: id_var @ 4...13} => {
        println!("Found an id in rage 4 to 13: {}", id_var);
    },
    /* some other arms */
}
````
