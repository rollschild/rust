use std::cell::RefCell;
use std::rc::{Rc, Weak};

#[derive(Debug)]
struct Node {
    value: i32,
    parent: RefCell<Weak<Node>>, // does not own the parent
    children: RefCell<Vec<Rc<Node>>>,
}

fn main() {
    let leaf = Rc::new(Node {
        value: 2037,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });

    println!("leaf's parent: {:?}", leaf.parent.borrow().upgrade());

    let branch = Rc::new(Node {
        value: 1026,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![Rc::clone(&leaf)]),
    });
    // now Node in leaf has two owners: leaf and branch

    *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

    println!("leaf's parent: {:?}", leaf.parent.borrow().upgrade());
}
