use std::collections::HashMap;

fn main() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Miko"), 89);
    scores.insert(String::from("Jovi"), 88);
    // scores.insert("Eva", 90); // would not work
    scores.entry(String::from("Jovi")).or_insert(100);
    scores.entry(String::from("Eva")).or_insert(99);
    println!("scores: {:?}", scores);

    let teams = vec!["Lakers", "Nuggets", "Nets"];
    let scores = vec![16, 0, 1];

    let history: HashMap<_, _> = teams.iter().zip(scores.iter()).collect();
    println!("histories: {:?}", history);
    let lakers = "Lakers";
    let champs: i32 = match history.get(&lakers) {
        Some(champs) => **champs,
        None => 0,
    };
    println!("how many lakers championships: {:?}", champs);

    let text = String::from("Everybody is looking for somebody's arms to fall in to");
    println!("word count for text: {:?}", word_count(&text[..]));
}

fn word_count(text: &str) -> HashMap<&str, i32> {
    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    return map;
}
