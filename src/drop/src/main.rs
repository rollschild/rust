struct CustomPointer {
    data: String,
}

impl Drop for CustomPointer {
    fn drop(&mut self) {
        println!("Dropping CustomPointer with data {}!", self.data);
    }
}

fn main() {
    let pointer_one = CustomPointer {
        data: String::from("it's pointer_one!"),
    }; // goes out of scope after pointer_two
    let pointer_two = CustomPointer {
        data: String::from("it's pointer_two!"),
    }; // goes out of scope first

    drop(pointer_one);

    println!("Two instances of CustomPointer created!");
}
