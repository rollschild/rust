enum List {
    /* Cons(i32, List), */
    Cons(i32, Box<List>),
    Nil,
} // recursive type

use List::{Cons, Nil};

fn main() {
    /* let list = Cons(3, Cons(2, Cons(1, Nil))); */
    let list = Cons(3, Box::new(Cons(2, Box::new(Cons(1, Box::new(Nil))))));
}
