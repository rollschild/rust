fn main() {
    let mut vector: Vec<i32> = vec![2037, -12, 0, 9999, 1024];
    let first = &mut vector[1];
    *first += 1;
    vector.push(7); // mutable borrow

    // loop though the vector
    for item in &mut vector {
        *item += 10;
    }
    println!("now the vector is: {:?}", vector);

    enum SpreadsheetCell {
        Int(i32),
        Text(String),
        Float(f64),
    };

    let row: Vec<SpreadsheetCell> = vec![
        SpreadsheetCell::Int(2037),
        SpreadsheetCell::Text(String::from("nothing forever")),
        SpreadsheetCell::Float(10.21),
    ];

    for cell in &row {
        match cell {
            SpreadsheetCell::Int(val) => {
                println!("an integer: {}", val);
            }
            SpreadsheetCell::Text(text) => {
                println!("words: {}", text);
            }
            SpreadsheetCell::Float(f) => {
                println!("floating... {}", f);
            }
        }
    }
}
