fn print_coords(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})!", x, y);
    println!("Current location: ({}, {})!", x, y);
    println!("Current location: ({}, {})!", x, y);
}

fn print_coords_own((x, y): (i32, i32)) {
    // println! does NOT take ownership
    println!("Current location: ({}, {})!", x, y);
    println!("Current location: ({}, {})!", x, y);
    println!("Current location: ({}, {})!", x, y);
}

fn main() {
    let fav_color: Option<&str> = None;
    let is_monday = false;
    let age: Result<u8, _> = "27".parse();

    if let Some(color) = fav_color {
        println!("{} is my favorite colr!", color);
    } else if is_monday {
        println!("today is Monday oh my god!");
    } else if let Ok(age) = age {
        // shadowing... haven't you found out?
        println!("I'm {} years old...", age);
    } else {
        println!("default...");
    }

    let point = (6, 8);
    print_coords(&point);
    print_coords_own(point);
}
