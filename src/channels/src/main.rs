use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();
    let tx_clone = mpsc::Sender::clone(&tx);

    thread::spawn(move || {
        let values = vec![
            String::from("This..."),
            String::from(" is "),
            String::from("a..."),
            String::from(" message!"),
        ];

        for val in values {
            tx.send(val).unwrap(); // Result<T, E>
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let values = vec![
            String::from(" and some..."),
            String::from("more messages"),
            String::from("on the way!"),
        ];

        for val in values {
            tx_clone.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    /* let received = rx.recv().unwrap(); */
    // we are not calling recv() here
    // ...because we are treating rx as an iterator
    for received in rx {
        println!("Received some message: {}", received);
    }
}
