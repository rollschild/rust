fn main() {
    let s = String::from("Hellohoney..");
    let word = find_first_word(&s); // immutable borrow
                                    // s.clear();
                                    // clear() truncates String - making a mutable reference
    println!("the first word is {}", word);

    let first_word = better_find_first_word(&s[..]);
    println!("first word: {}", first_word);

    let hello = "你好呀小白兔";
    // let h = &hello[0..2];
    let h = &hello[6..18];

    println!("Miko is {}", h);

    for ch in hello.chars() {
        println!("{}", ch);
    }
    for bt in hello.bytes() {
        println!("{}", bt);
    }
}

// notice the return type for string slice
fn better_find_first_word(string: &str) -> &str {
    let bytes = string.as_bytes();

    for (index, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &string[..index];
        }
    }

    return &string[..];
}

fn find_first_word(string: &String) -> &str {
    let bytes = string.as_bytes();

    for (index, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &string[..index];
        }
    }

    return &string[..];
}
