extern crate improved_minigrep;

use improved_minigrep::Config;
use std::env;
use std::process;

fn main() {
    // env::args() returns an iterator
    // we are passing the ownership of the iterator
    // ...to Config::new()
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Error parsing arguments: {}", err);
        process::exit(1); // exit the program immediately
    });
    println!("Searching for {}...", config.query);
    println!("In file {}", config.filename);

    if let Err(e) = improved_minigrep::run(&config) {
        eprintln!("Application Error: {}", e);
        process::exit(1);
    }
}
