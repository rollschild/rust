extern crate blog;
use blog::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("Today is Saturday...");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    assert_eq!("Today is Saturday...", post.content());
}
