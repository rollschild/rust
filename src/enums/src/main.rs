#[derive(Debug)]
enum UsaStates {
    Georgia,
    Ohio,
    Michigan,
    Portland,
    SouthCarolina,
    NorthCarolina,
    Virginia,
    WashingtonDC,
    Maryland,
    NewYork,
    Massachusets,
    California,
    Colorado,
    Kansas,
    Missouri,
}

enum Coin {
    Penny,
    Dime,
    Nickel,
    Quarter(UsaStates),
}

fn main() {
    enum IpAddr {
        V4(u8, u8, u8, u8),
        V6(String),
    };

    let home = IpAddr::V4(127, 0, 0, 1);
    let loopback = IpAddr::V6(String::from("::1"));

    let absent_number: Option<i32> = None;

    let value = value_in_cents(Coin::Quarter(UsaStates::WashingtonDC));
    println!("value in cents is {}", value);

    let some_u8_value = Some(0u8);
    if let Some(3) = some_u8_value {
        println!("three!!!");
    }
}

fn value_in_cents(coin: Coin) -> u32 {
    return match coin {
        Coin::Penny => 1,
        Coin::Dime => 5,
        Coin::Nickel => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}", state);
            return 25;
        }
    };
}
