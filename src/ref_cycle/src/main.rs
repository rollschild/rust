use std::cell::RefCell;
use std::rc::Rc;
use List::{Cons, Nil};

#[derive(Debug)]
enum List {
    Cons(i32, RefCell<Rc<List>>),
    Nil,
}

impl List {
    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
        match *self {
            Cons(_, ref item) => Some(item),
            Nil => None,
        }
    }
}

fn main() {
    let cons_list_ref = Rc::new(Cons(2037, RefCell::new(Rc::new(Nil))));
    println!(
        "cons_list_ref initial RC count = {}",
        Rc::strong_count(&cons_list_ref)
    ); // 1
    println!("cons_list_ref tail = {:?}", cons_list_ref.tail());

    let alt_cons_ref = Rc::new(Cons(1026, RefCell::new(Rc::clone(&cons_list_ref))));
    println!(
        "cons_list_ref RC count after alt_cons_ref created = {}", // 2
        Rc::strong_count(&cons_list_ref)
    );
    println!(
        "alt_cons_ref initial RC count = {}",
        Rc::strong_count(&alt_cons_ref)
    ); // 1
    println!("alt_cons_ref tail = {:?}", alt_cons_ref.tail());

    if let Some(link) = cons_list_ref.tail() {
        // link now is ref to the RefCell<Rc<List>> in cons_list_ref
        // change from Rc<List> that holds Nil
        // ...to the Rc<List> in alt_cons_ref
        *link.borrow_mut() = Rc::clone(&alt_cons_ref);
    }
    println!(
        "RC count of alt_cons_ref after changing cons_list_ref = {}",
        Rc::strong_count(&alt_cons_ref)
    ); // 2
    println!(
        "RC count of cons_list_ref after changing itself = {}",
        Rc::strong_count(&cons_list_ref)
    ); // 2

    /* THIS WILL OVERFLOOWWWWWWWW THE STACKKKKKKKK */
    /* println!("cons_list_ref.tail is: {:?}", cons_list_ref.tail()); */
}
