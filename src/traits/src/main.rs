use traits::notify;
use traits::NewsArticle;
use traits::Pair;
use traits::Summary;
use traits::Tweet;

fn main() {
    let tweet = Tweet {
        username: String::from("rollschild"),
        content: String::from("this is my first tweet..."),
        reply: false,
        retweet: false,
    };
    println!("1 new tweet: {}", tweet.summerize());

    let article = NewsArticle {
        headline: String::from("Look At This!"),
        location: String::from("New York City"),
        author: String::from("Guangchu Shi"),
        content: String::from("New York is a good city and I want to live here..."),
    };
    println!("New article came out! {}", article.summerize());

    // make sure generic type T does implement
    // ...method summerize
    notify(&article);

    let pair = Pair { x: 15, y: 14 };
    pair.cmp_display();
}
