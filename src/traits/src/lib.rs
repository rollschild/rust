use std::fmt::Display;

pub struct Pair<T> {
    pub x: T,
    pub y: T,
}

impl<T> Pair<T> {
    pub fn new(x: T, y: T) -> Self {
        return Self { x, y };
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    pub fn cmp_display(&self) {
        if &self.x > &self.y {
            println!("x = {} is larger!", &self.x);
        } else {
            println!("y = {} is larger!", &self.y);
        }
    }
}

pub trait Summary {
    fn summerize_author(&self) -> String;
    fn summerize(&self) -> String {
        return format!("(Read more from {}...)", &self.summerize_author());
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summerize_author(&self) -> String {
        return String::from(&self.author);
    }
    fn summerize(&self) -> String {
        return format!(
            "{}, by {} from {}",
            &self.headline, &self.author, &self.location
        );
    }
}

pub fn notify<T: Summary>(item: &T) {
    // trait bounds
    // T can be any type that implements Summary
    println!("Breaking news! {}", item.summerize());
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,   // i32?
    pub retweet: bool, // i32?
}

impl Summary for Tweet {
    fn summerize_author(&self) -> String {
        return format!("@{}", &self.username);
    }
    /* fn summerize(&self) -> String {
     *     return format!("@{} - {}", &self.username, &self.content);
     * } */
}
