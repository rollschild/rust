fn main() {
    struct Point {
        x: i32,
        y: i32,
    };

    let points = vec![
        Point { x: 0, y: 0 },
        Point { x: -6, y: 12 },
        Point { x: 3, y: 4 },
        Point { x: 6, y: -8 },
    ];

    let sum_of_squares: i32 = points.iter().map(|&Point { x, y }| x * x + y * y).sum();

    println!("sum of the squares of x and y: {}", sum_of_squares);
}
