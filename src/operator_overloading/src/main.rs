use std::ops::Add;

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, PartialEq)]
struct Millimeters(u32);
#[derive(Debug, PartialEq)]
struct Meters(u32);

impl Add<Meters> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        return Millimeters(self.0 + other.0 * 1000);
    }
}

impl Add for Point {
    // I believe this Output is an implicit required type
    type Output = Point;

    // Add trait expects Point, NOT &Point
    fn add(self, other: Point) -> Point {
        return Point {
            x: self.x + other.x,
            y: self.y + other.y,
        };
    }
}

fn main() {
    assert_eq!(
        Point { x: -2, y: 6 } + Point { x: 8, y: 9 },
        Point { x: 6, y: 15 }
    );

    assert_eq!(Millimeters(12) + Meters(1), Millimeters(1012));
}
