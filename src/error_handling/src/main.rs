use std::fs::File;
use std::io;
use std::io::ErrorKind;
use std::io::Read;

fn main() {
    let f = File::open("test_error.rs"); // outside of src/
    let f = match f {
        Ok(file) => file,
        Err(ref error) if error.kind() == ErrorKind::NotFound => {
            match File::create("test_error.rs") {
                Ok(fh) => {
                    println!("creating new file test_error.rs...");
                    fh
                }
                Err(err) => {
                    panic!("Error creating file: {:?}", err);
                }
            }
        }
        Err(error) => {
            panic!("Error opening file: {:?}", error);
        }
    };

    let user_name = read_username_from_file().expect("Error reading username...");
    println!("user name is: {}", user_name);
    /* match user_name {
     *     Ok(name) => println!("username is: {}", name),
     *     Err(err) => panic!("Error reading user name: {:?}", err),
     * } */
}

fn read_username_from_file() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("username.txt")?.read_to_string(&mut s)?;
    return Ok(s);
}
