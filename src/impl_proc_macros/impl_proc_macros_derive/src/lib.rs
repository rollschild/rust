// procedural macros need to be in their own crate

// convert Rust code to a string containing Rust code
extern crate proc_macro;
// parse string of Rust code to a data structure
extern crate syn;
// takes syn data structure and turns them back to Rust code
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;

// this function will get called
#[proc_macro_derive(ImplProcMacros)]
pub fn impl_proc_macros_derive(input: TokenStream) -> TokenStream {
    let token_stream: String = input.to_string();

    // returns a DeriveInput struct
    let ast = syn::parse_derive_input(&token_stream).unwrap();
    /*
     * DeriveInput {
     *     ident: Ident {
     *         "Camels"
     *     },
     *     body: Struct {
     *         Unit
     *     }
     * }
     */

    let handle = impl_proc_macros_greetings(&ast);

    // use panic! or expect for better usage
    return handle.parse().unwrap();
    // the returned TokenStream is added to the code that we (crate users) write, at compile time
}

fn impl_proc_macros_greetings(ast: &syn::DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    quote! {
        impl ImplProcMacros for #name {
            fn proc_macros_greetings() {
                println!("This is a procedural macro {}! What's up!", stringify!(#name));
                // stringify takes a Rust expression and convert it into a string literal
            }
        }
    }
}
