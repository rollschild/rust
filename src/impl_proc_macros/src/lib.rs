pub trait ImplProcMacros {
    fn proc_macros_greetings();
}

/* we could do this instead:
 * impl ImplProcMacros for Camels {
 *     // code
 * }
 */
