extern crate gui;
use gui::{Button, Draw, Screen};

struct SelectBox {
    width: u32,
    height: u32,
    options: Vec<String>,
}

impl Draw for SelectBox {
    fn draw(&self) {
        println!("drawing a select box...");
    }
}

fn main() {
    let screen = Screen {
        components: vec![
            Box::new(Button {
                width: 3,
                height: 5,
                label: String::from("Exit!"),
            }),
            Box::new(SelectBox {
                width: 6,
                height: 10,
                options: vec![
                    String::from("Take a look!"),
                    String::from("No thanks..."),
                    String::from("Report!"),
                ],
            }),
        ],
    };

    screen.run();
}
