// define a trait Draw
pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    // Box<Draw> is a trait object
    // this allows for multiple concrete types to fill in for
    // ...the trait object at runtime
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    // not part of the public API?
    fn draw(&self) {
        println!("drawing a button...");
    }
}
