fn main() {
    // let quotient: f32 = 20.37 / 3.2; // 6.365624
    let quotient: f64 = 20.37 / 3.2; // 6.365625
    println!("20.37 / 3.2 is {}", quotient);

    let tup: (i64, f32, char) = (500, 20.37, 'j');
    let (x, y, z) = tup; // destructuring
    println!("members of tup are {}, {}, and {}.", x, y, z);
}
