#[derive(Debug)]
struct Rectangle {
    height: u32,
    width: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        return self.height * self.width;
    }

    fn can_hold(&self, other_rect: &Rectangle) -> bool {
        return self.height > other_rect.height && self.width > other_rect.width;
    }

    fn square(size: u32) -> Rectangle {
        return Rectangle {
            height: size,
            width: size,
        };
    }
}

fn main() {
    let rect: (u32, u32) = (4, 5);
    let rectan = Rectangle {
        height: 6,
        width: 7,
    };
    let another_rect = Rectangle {
        height: 10,
        width: 14,
    };
    let third_rect = Rectangle {
        height: 12,
        width: 9,
    };
    let square = Rectangle::square(17);

    println!("area of rectangle is {}", area_tuple(rect));
    println!(
        "area of another rectangle `{:#?}` is {}",
        rectan,
        area_struct(&rectan)
    );
    println!(
        "area of rectangle `{:#?}` is {}",
        another_rect,
        another_rect.area()
    );
    println!(
        "rectangle {:#?} can hold rectangle {:#?}? {}",
        another_rect,
        third_rect,
        another_rect.can_hold(&third_rect)
    );
    println!(
        "rectangle {:#?} can hold rectangle {:#?}? {}",
        another_rect,
        rectan,
        another_rect.can_hold(&rectan)
    );
    println!("square {:#?} has area size: {}", square, square.area());
}

fn area_tuple(dimesion: (u32, u32)) -> u32 {
    return dimesion.0 * dimesion.1;
}

fn area_struct(rectangle: &Rectangle) -> u32 {
    return rectangle.height * rectangle.width;
}
