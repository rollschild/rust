use std::rc::Rc;

enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use List::{Cons, Nil};

fn main() {
    let pivot = Rc::new(Cons(5, Rc::new(Cons(7, Rc::new(Nil)))));
    println!("Count after creating pivot: {}!", Rc::strong_count(&pivot));
    // 1

    let arm_one = Cons(3, Rc::clone(&pivot));
    println!(
        "Count after creating arm_one: {}!",
        Rc::strong_count(&pivot)
    ); // 2
    {
        let arm_two = Cons(9, Rc::clone(&pivot));
        println!(
            "Count after creating arm_two: {}!",
            Rc::strong_count(&pivot)
        ); // 3
    }
    println!(
        "Count after arm_two goes out of scope: {}!",
        Rc::strong_count(&pivot)
    ); // 2
}
