fn main() {
    let x: i32 = 5;

    let y = {
        let x = 33;
        x + 6
    };

    println!("x now is: {}", x); // 5
    println!("y now is: {}", y); // 39
    println!("x + 1 is: {}", plus_one(x));

    let array = [9, 8, 7, 6, 5];
    let mut index = 0;

    // error prone and slow
    while index < 5 {
        println!("index {} of array is {}", index, array[index]);
        index = index + 1;
    }

    // better solution:
    for element in array.iter() {
        println!("value of array is {}", element);
    }

    // Range: right bound exclusive
    for number in (-6..18).rev() {
        println!("counting down.. {}", number);
    }
}

fn plus_one(x: i32) -> i32 {
    return x + 1;
}
