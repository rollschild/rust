pub struct Rectangle {
    length: i32,
    width: i32,
}

impl Rectangle {
    pub fn can_hold(&self, other: &Rectangle) -> bool {
        return self.length > other.length && self.width > other.width;
    }
}

pub struct Guess {
    value: u32,
}

impl Guess {
    pub fn new(val: u32) -> Guess {
        if val < 1 {
            panic!(
                "Guess value must be greater than or equal to 1; received {} instead",
                val
            );
        } else if val > 100 {
            panic!(
                "Guess value must be smaller than or equal to 100; received {} instead",
                val
            );
        }

        return Guess { value: val };
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    // tests module is an inner module
    // we need to bring outer module in

    #[test]
    #[ignore]
    fn large_hold_small() {
        let larger = Rectangle {
            length: 13,
            width: 9,
        };
        let smaller = Rectangle {
            length: 12,
            width: 8,
        };
        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn small_cannot_hold_large() {
        let larger = Rectangle {
            length: 13,
            width: 9,
        };
        let smaller = Rectangle {
            length: 12,
            width: 10,
        };
        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    #[should_panic(expected = "Guess value must be smaller than or equal to 100")]
    // substring is fine

    fn success_too_large() {
        Guess::new(101);
    }
}

/* #[cfg(test)]
 * mod tests {
 *     #[test]
 *     fn it_works() {
 *         assert_eq!(2 + 2, 4);
 *     }
 *
 *     #[test]
 *     fn this_will_fail() {
 *         panic!("This failed!");
 *     }
 * } */
