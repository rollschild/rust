extern crate adder;

mod common;

#[test]
#[should_panic]
fn guess() {
    common::setup();
    adder::Guess::new(0);
}
