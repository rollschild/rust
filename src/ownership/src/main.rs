fn main() {
    let mut s = String::from("Hey there..");
    s.push_str(", and what's up?"); // append
    println!("{}", s);

    let copy_of_s = s; // s became invalid after this
    println!("s after copying: {}", copy_of_s);

    let clone_of_s = copy_of_s.clone();
    println!("copy_of_s: {}\nclone_of_s: {}", copy_of_s, clone_of_s);

    let s1 = gives_ownership();
    let s2 = String::from("huh..");

    // here s2 is moved into s3 and becomes invalid
    let s3 = takes_then_gives_back(s2);

    println!("s1 now is: {}", s1);
    println!("s3 now is: {}", s3);

    let new_str = String::from("this is a new! string..");
    let (returned_str, len) = compute_len(new_str);
    println!("length of '{}' is: {}", returned_str, len);

    let has_ref_str = String::from("this string has a reference");
    let length = calculate_len(&has_ref_str);
    println!("string '{}' has a length of {}", has_ref_str, length);

    let mut change_str = String::from("hello");
    // create a mutable reference:
    change(&mut change_str);
}

// accept a mutable reference:
fn change(some_str: &mut String) {
    some_str.push_str(", world!");
}

fn gives_ownership() -> String {
    let some_string = String::from("this is s1!");
    return some_string;
}

fn takes_then_gives_back(some_string: String) -> String {
    return some_string;
}

fn compute_len(new_str: String) -> (String, usize) {
    let len = new_str.len();

    // can return a tuple
    return (new_str, len);
}

fn calculate_len(ref_to_str: &String) -> usize {
    return ref_to_str.len();
}
