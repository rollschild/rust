extern crate rand; // external dependency

use rand::Rng;
use std::cmp::Ordering;
// Ordering is another type brought into scope
use std::io;

pub struct Guess {
    value: u32,
}

impl Guess {
    // associated function; use :: to access
    pub fn new(value: u32) -> Guess {
        if value < 1 || value > 100 {
            panic!("guess {} out of bound!", value);
        }

        return Guess { value };
    }

    // a getter
    pub fn value(&self) -> u32 {
        return self.value;
    }
}

fn main() {
    println!("Let's begin a guessing game!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Enter your guess please...");

        // returns a new instance of String
        let mut guess = String::new();
        // String is UTF-8 encoded
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line!");

        let guess: Guess = match guess.trim().parse() {
            Ok(num) => Guess::new(num),
            Err(_) => {
                println!("Invalid input! restarting...");
                continue;
            }
        };

        /* if guess < 1 || guess > 100 {
         *     println!("your guess is out of bound. Guess a new one!");
         *     continue;
         * } */

        match guess.value().cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too large!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }

        println!("You guessed: {}", guess.value());
        // placeholder
        // println!("The secret number is: {}", secret_number);
    }
    println!("The secret number is: {}", secret_number);
}
