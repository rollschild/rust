struct Context<'a>(&'a str);

struct Parser<'a, 'b> {
    context: &'a Context<'b>,
}
/* Parser holds a reference to Context with lifetime 'b
 * and that Context holds a string slice which also lives
 * as long as the reference to the Context in Parser
 */

impl<'c, 's> Parser<'c, 's> {
    fn parse(&self) -> Result<(), &'s str> {
        // pretend the input is invalid after the first byte???
        return Err(&self.context.0[1..]);
    }
}

fn parse_context(context: Context) -> Result<(), &str> {
    return Parser { context: &context }.parse();
}

fn main() {
    parse_context(Context("hello honey")).unwrap();
}
