use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
enum List {
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

use List::{Cons, Nil};

fn main() {
    let value = Rc::new(RefCell::new(2037));

    let cons_list = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let alt_cons_one = Cons(Rc::new(RefCell::new(13)), Rc::clone(&cons_list));
    let alt_cons_two = Cons(Rc::new(RefCell::new(109)), Rc::clone(&cons_list));

    println!("cons_list: {:?}", cons_list);
    println!("alt_cons_one: {:?}", alt_cons_one);
    println!("alt_cons_two: {:?}", alt_cons_two);
}
