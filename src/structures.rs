#[derive(Debug)]
struct Person<'a> {
    name: &'a str,
    age: u8,
}

// unit struct
struct Nil;

// tuple struct
struct Pair(i32, f32);

struct Point {
    x: f32,
    y: f32,
}

struct Rectangle {
    p1: Point,
    p2: Point,
}

fn main() {
    let name = "Jovi";
    let age = 25;
    let jovi = Person { name, age };

    println!("{:?}", jovi);
}
