extern crate communicator;

use communicator::client;
// we can also directly bring the function into scope:
use communicator::network::server::connect;

enum TrafficLight {
    Red,
    Yellow,
    Green,
}

use TrafficLight::{Green, Yellow};
// use TrafficLight::*;

fn main() {
    client::connect();
    connect();

    let yellow = Yellow;
    let green = Green;
    let red = TrafficLight::Red;
}
