#[derive(PartialEq, Debug)]
struct Shoe {
    size: u32,
    style: String,
}

fn shoes_in_my_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
    return shoes
        .into_iter()
        .filter(|shoe| shoe.size == shoe_size)
        .collect();
}

#[test]
fn filters_by_size() {
    let shoes = vec![
        Shoe {
            size: 11,
            style: String::from("basketball"),
        },
        Shoe {
            size: 12,
            style: String::from("basketball"),
        },
        Shoe {
            size: 11,
            style: String::from("boots"),
        },
        Shoe {
            size: 10,
            style: String::from("hiking"),
        },
        Shoe {
            size: 11,
            style: String::from("trail running"),
        },
    ];

    let my_shoes = shoes_in_my_size(shoes, 11);

    assert_eq!(
        my_shoes,
        vec![
            Shoe {
                size: 11,
                style: String::from("basketball"),
            },
            Shoe {
                size: 11,
                style: String::from("boots"),
            },
            Shoe {
                size: 11,
                style: String::from("trail running"),
            },
        ]
    );
}

struct Counter {
    count: u32,
}

impl Counter {
    fn new() -> Counter {
        return Counter { count: 0 };
    }
}

impl Iterator for Counter {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        self.count += 1;

        return if self.count < 6 {
            Some(self.count)
        } else {
            None
        };
    }
}

#[test]
fn calling_next_directly() {
    let mut counter = Counter::new();

    assert_eq!(counter.next(), Some(1));
    assert_eq!(counter.next(), Some(2));
    assert_eq!(counter.next(), Some(3));
    assert_eq!(counter.next(), Some(4));
    assert_eq!(counter.next(), Some(5));
    assert_eq!(counter.next(), None);
}

#[test]
fn sum_weird() {
    let sum: u32 = Counter::new()
        .zip(Counter::new().skip(1))
        .map(|(x, y)| x * y)
        .filter(|result| result % 3 == 0)
        .sum();

    assert_eq!(sum, 18);
}
