/* inside src/bin/
 * run `cargo run`
 */
extern crate web_server;

use std::fs::File;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;
use std::time::Duration;
use web_server::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    // would just stop the program if binding fails

    let pool = ThreadPool::new(6);

    // we are iterating over connection attempts
    for stream in listener.incoming().take(3) {
        // streams of type TcpStream
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    } // stream goes out of scope here
      // and the connection is closed
    println!("Shutting down...");
} // ThreadPool goes out of scope here
  // ...and the drop() method runs

fn handle_connection(mut stream: TcpStream) {
    // it's an array, which is a primitive type
    // NOT a vector
    let mut buffer: [u8; 512] = [0; 512];

    // read from the stream
    // request data (raw bytes) in buffer
    stream.read(&mut buffer).unwrap();
    /* println!("Request: {}", String::from_utf8_lossy(&buffer[..])); */
    // replace invalid sequence with U+FFFD REPLACEMENT CHARACTER

    let get_request = b"GET / HTTP/1.1\r\n";
    // &[u8]

    let sleep_response = b"GET /sleep HTTP/1.1\r\n";

    let (status_line, filename) = if buffer.starts_with(get_request) {
        ("HTTP/1.1 200 OK\r\n\r\n", "contents.html")
    } else if buffer.starts_with(sleep_response) {
        thread::sleep(Duration::from_secs(6));
        ("HTTP/1.1 200 OK\r\n\r\n", "contents.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
    };

    let mut file = File::open(filename).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    // add HTML to response as a body
    let response = format!("{}{}", status_line, contents);
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
