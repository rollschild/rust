use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

enum Signal {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Signal>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    /// `size` is number of threads
    ///
    /// # Panics
    /// the `new` function will panic if the size is zero
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            // create workers
            workers.push(Worker::new(id, Arc::clone(&receiver)));
            // Arc::clone bumps the reference count
        }

        return ThreadPool { workers, sender };
    }

    pub fn execute<F>(&self, f: F)
    where
        // FnOnce represents a closure and does not return
        // the return type can be omitted from signature
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);
        self.sender.send(Signal::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending TERMINATE signal to all workers...");

        for _ in &mut self.workers {
            self.sender.send(Signal::Terminate).unwrap();
        }

        println!("Shutting down all workers...");

        for worker in &mut self.workers {
            println!("Shutting down worker {}...", worker.id);
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
    // the closure created by spawn does not return anything
    // thus the ()
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Signal>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let signal = receiver.lock().unwrap().recv().unwrap();
            // call lock() on receiver to acquire mutex
            // call recv() to receive a Job from the channel
            // the recv() call blocks!!!
            // if no Job yet, current thread will wait until a job is available
            // Mutex ensures only one Worker thread at a time is requesting a job
            // the lock drops as soon as this statement ends
            // ...allows for multiple requests being serviced concurrently

            match signal {
                Signal::NewJob(job) => {
                    println!("Worker {} received a job and is executing!", id);
                    /* (*job)(); */
                    job.call_box();
                }
                Signal::Terminate => {
                    println!("Worker {} was asked to terminate!", id);
                    break;
                }
            }
        });
        return Worker {
            id,
            thread: Some(thread),
        };
    }
}

/* struct Job; */
type Job = Box<dyn FnBox + Send + 'static>;

trait FnBox {
    fn call_box(self: Box<Self>);
    // take ownership of `self` and move the value out of `Box<T>`
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)();
    }
}
