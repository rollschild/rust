extern crate impl_proc_macros;
#[macro_use]
extern crate impl_proc_macros_derive;

use impl_proc_macros::ImplProcMacros;

// get a default implementation of the impl_proc_macros() function
// ...for the type we defined
#[derive(ImplProcMacros)]
struct Camels;

fn main() {
    Camels::proc_macros_greetings();
}
