fn largest_i32(list: &[i32]) -> i32 {
    // any concrete slice of i32 values
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    return largest;
}

fn largest_char(list: &[char]) -> char {
    // any concrete slice of i32 values
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    return largest;
}

fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    return largest;
}

fn largest_ref<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    // Note that iter() iterates over &T
    for item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    return largest;
}

// Notice where I put this derive thing...
#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        return &self.x;
    }
}
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        return (&self.x.powi(2) + &self.y.powi(2)).sqrt();
    }
}

#[derive(Debug)]
struct MultiPoint<T, U> {
    x: T,
    y: U,
}

impl<T, U> MultiPoint<T, U> {
    fn mixup<V, W>(self, other: MultiPoint<V, W>) -> MultiPoint<T, W> {
        return MultiPoint {
            x: self.x,
            y: other.y,
        };
    }
}

fn main() {
    let number_list = vec![-2037, 100, 0, 99, 6];
    // let result = largest_i32(&number_list);
    let result = largest(&number_list);
    println!("largest number in {:?} is {}", number_list, result);

    let number_list = vec![12, 5, -1, 7, 13, 9, 12];
    // let result = largest_i32(&number_list);
    let result = largest_ref(&number_list);
    println!("largest number in {:?} is {}", number_list, result);

    let char_list = vec![
        'i', 'l', 'o', 'v', 'e', 'b', 'a', 's', 'k', 'e', 't', 'b', 'a', 'l', 'l',
    ];
    // let result = largest_char(&char_list);
    let result = largest(&char_list);
    println!("largest char in {:?} is {}", char_list, result);

    let point = Point { x: 12.7, y: 0.6 };
    let x_coord = point.x();
    let dist = point.distance_from_origin();
    println!("x coordinator of Point {:?} is {}!", point, x_coord);
    println!("distance of Point {:?} from origin is: {}", point, dist);

    let p1 = MultiPoint { x: 12.6, y: 9 };
    let p2 = MultiPoint { x: "Hello", y: 'c' };
    let p3 = p1.mixup(p2); // p1 and p2 are moved and thus invalid
                           /* println!("{:?}", p2); */
    println!("mix of p1 and p2 is: {:?}", p3);
}
