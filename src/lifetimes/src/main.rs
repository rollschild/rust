use std::fmt::Display;

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        return x;
    } else {
        return y;
    }
}

fn longest_owned(x: &str, y: &str) -> String {
    let result = String::from("really longggggg");
    return result;
}

fn longest_announce<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
where
    T: Display,
{
    println!("Attention! {}", ann);
    println!("Attention again... {}", ann);
    if x.len() > y.len() {
        return x;
    } else {
        return y;
    }
}

#[derive(Debug)]
struct NoName<'a> {
    part: &'a str,
}

impl<'a> NoName<'a> {
    fn announce(&self, announcement: &str) -> &str {
        // third ellision rule
        println!("Attention please: {}", announcement);
        return &self.part;
    }
}

fn main() {
    let str1 = String::from("shorter...");
    {
        let str2 = String::from("I am longer..");
        let result = longest(str1.as_str(), str2.as_str());
        println!("longer string is {}", result);
    }
    let str2 = String::from("doesn't matter");
    let result_owned = longest_owned(str1.as_str(), str2.as_str());
    println!("longest is {}", result_owned);

    let novel_name = String::from("A brief history of seven killings..");
    let mut itr = novel_name.split(' ');
    let _ = itr.next().expect("No word existing basically!");
    // a reference
    let second_word = itr.next().expect("Could not find an empty space!");
    let instance = NoName { part: second_word };
    println!("{:?}", instance);
    let words = instance.announce("I am married!");
    println!("words from announcement: {}", words);

    let str1 = String::from("doesn't matterr");
    let new_res = longest_announce(str1.as_str(), str2.as_str(), "shit...");
    println!("Which is longer? {}", new_res);
}
