use std::collections::HashMap;
use std::thread;
use std::time::Duration;

/* struct Cache<T>
 * where
 *     T: Fn(u32) -> u32,
 * {
 *     calculation: T,
 *     value: Option<u32>,
 * }
 *
 * impl<T> Cache<T>
 * where
 *     T: Fn(u32) -> u32,
 * {
 *     fn new(calculation: T) -> Cache<T> {
 *         return Cache {
 *             calculation,
 *             value: None,
 *         };
 *     }
 *
 *     fn value(&mut self, arg: u32) -> u32 {
 *         match self.value {
 *             Some(val) => val,
 *             None => {
 *                 let val = (self.calculation)(arg);
 *                 self.value = Some(val);
 *                 return val;
 *             }
 *         }
 *     }
 * } */

struct ModifiableCache<T>
where
    T: Fn(u32) -> u32,
{
    calculation: T,
    dictionary: HashMap<u32, u32>,
}

impl<T> ModifiableCache<T>
where
    T: Fn(u32) -> u32,
{
    fn new(calculation: T) -> ModifiableCache<T> {
        return ModifiableCache {
            calculation,
            dictionary: HashMap::new(),
        };
    }

    fn value(&mut self, arg: u32) -> u32 {
        if self.dictionary.contains_key(&arg) {
            return self.dictionary[&arg];
        } else {
            let val = (self.calculation)(arg);
            self.dictionary.insert(arg, val);
            return val;
        }
        /* return self
         *     .dictionary
         *     .entry(arg)
         *     .or_insert((self.calculation)(arg)); */
    }
}

fn main() {
    let simulated_user_input = 17;
    let simulated_random_number = 7;

    generate_workout(simulated_user_input, simulated_random_number);
}

fn generate_workout(intensity: u32, random_number: u32) {
    let mut expensive_result = ModifiableCache::new(|num| {
        println!("calculating slowly...");
        thread::sleep(Duration::from_secs(6));
        return num;
    });

    if intensity < 23 {
        println!("Do {} pushups!", expensive_result.value(intensity));
        println!("...then do {} situps!", expensive_result.value(intensity));
    } else {
        if random_number == 3 {
            println!("Today we take a rest...");
        } else {
            println!("Run for {} minutes!", expensive_result.value(intensity));
        }
    }
}
