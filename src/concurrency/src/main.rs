use std::thread;
use std::time::Duration;

fn main() {
    let numbers = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

    // NO guarantee on the order in which threads run
    let handle = thread::spawn(move || {
        for i in numbers {
            println!("spawned thread: {}!", i);
            thread::sleep(Duration::from_secs(1));
        }
    });

    for j in 1..7 {
        println!("main thread: {}!", j);
        thread::sleep(Duration::from_secs(1));
    }

    handle.join().unwrap();
}
