extern crate minigrep;

use minigrep::Config;
use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    // includes the binary itself
    // has to be valid Unicode

    // let config = parse_config(&args);
    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Error parsing arguments: {}", err);
        process::exit(1); // exit the program immediately
    });
    println!("Searching for {}...", config.query);
    println!("In file {}", config.filename);

    // we do not need unwrap_or_else() here
    // ...because we have nothing to unwrap, if successful
    // we prefix run() with the crate name
    //
    if let Err(e) = minigrep::run(&config) {
        eprintln!("Application Error: {}", e);
        process::exit(1);
    }
    /*     let mut f = File::open(config.filename).expect("Error opening file");
     *     let mut contents = String::new();
     *     f.read_to_string(&mut contents)
     *         .expect("Error reading file to string!");
     *
     *     println!("The poem is as follows:\n{}", contents); */
}

/* fn parse_config(args: &[String]) -> Config {
 *     // these two are configuration variables
 *     let query = &args[1];
 *     let filename = &args[2];
 *
 *     return Config { query, filename };
 * } */
