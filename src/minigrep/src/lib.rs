/* Public API */
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "tumor";
        let contents = "\
I heard of a man
who says words so beautifully
that if he only speaks their name
women give themselves to him.

If I am dumb beside your body
while silence blossoms like tumors on our lips.
it is because I hear a man climb stairs and clear his throat outside the door.";

        assert_eq!(
            vec!["while silence blossoms like tumors on our lips."],
            search_case_sensitive(query, contents)
        );
    }

    #[test]
    fn case_insensitive() {
        let query = "tUMoR";
        let contents = "\
I heard of a man
who says words so beautifully
that if he only speaks their name
women give themselves to him.

If I am dumb beside your body
while silence blossoms like tumors on our lips.
it is because I hear a man climb stairs and clear his throat outside the door.";

        assert_eq!(
            vec!["while silence blossoms like tumors on our lips."],
            search_case_insensitive(query, contents)
        );
    }

}

pub struct Config<'a> {
    pub query: &'a str,
    pub filename: &'a str,
    pub case_sensitive: bool, // need to initialize this
}

pub fn run(config: &Config) -> Result<(), Box<Error>> {
    let mut f = File::open(config.filename)?; // returns early
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    let results = if config.case_sensitive {
        // cannot use return here
        search_case_sensitive(config.query, &contents)
    } else {
        search_case_insensitive(config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }
    return Ok(()); // unit type
}

impl<'a> Config<'a> {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("Not enough arguments!");
        }
        if args.len() > 3 {
            return Err("Too many arguments!");
        }
        let query = &args[1];
        let filename = &args[2];

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
        //                   ^^^ this returns Result
        return Ok(Config {
            query,
            filename,
            case_sensitive,
        });
    }
}

pub fn search_case_sensitive<'l>(query: &str, contents: &'l str) -> Vec<&'l str> {
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }
    return results;
}

pub fn search_case_insensitive<'l>(query: &str, contents: &'l str) -> Vec<&'l str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }
    return results;
}
