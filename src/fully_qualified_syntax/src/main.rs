trait Animal {
    // associated function?
    // NOT a method
    // does NOT have a self parameter
    fn baby_name() -> String;
}

struct Dog;

impl Dog {
    fn baby_name() -> String {
        return String::from("Dog!");
    }
}

impl Animal for Dog {
    fn baby_name() -> String {
        return String::from("Puppy!");
    }
}

fn main() {
    println!("Dog's baby_name is: {}", Dog::baby_name());
    println!(
        "Dog as an Animal - its baby_name is: {}",
        <Dog as Animal>::baby_name()
    );
}
