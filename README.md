# The Rust Programming Language

My learning progress of Rust.

Rust is an _ahead-of-time compiled_ language.  
Rust has a strong, static type system.  
However, Rust also has type inference.  

By default variables are *immutable*.

[crates.io](https://crates.io/)
